﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace NganHang_2180604179_LeThanhTruc.Models
{

    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        // Navigation properties
        public ICollection<Transaction> Transactions { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}