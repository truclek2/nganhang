﻿using Microsoft.AspNetCore.Mvc;

namespace NganHang_2180604179_LeThanhTruc.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }

        // Navigation properties
        public ICollection<Transaction> Transactions { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}
