﻿using Microsoft.AspNetCore.Mvc;
using NganHang_2180604179_LeThanhTruc.Models;

namespace NganHang_2180604179_LeThanhTruc.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransactionDate { get; set; }

        // Navigation properties
        public Customer Customer { get; set; }
        public Employee Employee { get; set; }
        public ICollection<Log> Logs { get; set; }
    }
}