﻿using Microsoft.AspNetCore.Mvc;

namespace NganHang_2180604179_LeThanhTruc.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int TransactionId { get; set; }
        public DateTime LogDate { get; set; }
        public string Message { get; set; }

        // Navigation properties
        public Transaction Transaction { get; set; }
    }
}