﻿using Microsoft.AspNetCore.Mvc;
using NganHang_2180604179_LeThanhTruc.Models;

namespace NganHang_2180604179_LeThanhTruc.Models
{
    public class Account
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AccountName { get; set; }

        // Navigation properties
        public Customer Customer { get; set; }
        public ICollection<Report> Reports { get; set; }
    }
}
